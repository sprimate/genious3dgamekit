using UnityEngine;

namespace Gamekit3D.GameCommands
{
    public class RespawnPlayer : GameCommandHandler
    {
        public Gamekit3D.PlayerController player;

        public override void PerformInteraction()
        {
            player.Kill();
            //Debug.Log("player.Kill() couldn't be found?");
        }
    }
}